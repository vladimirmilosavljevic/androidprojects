package com.eng.katalog

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_model.view.*
import kotlinx.android.synthetic.main.moja_celija_logotip.view.*
import kotlinx.android.synthetic.main.moja_celija_logotip.view.model
import kotlinx.android.synthetic.main.moja_celija_model.view.*

class MojAdapterModel(val preuzetiModeli: ArrayList<Model>) :
    RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.moja_celija_model, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {

        holder.view.tvmodel.text = preuzetiModeli[position].marka
        holder.view.cena.text = "Cena:" + preuzetiModeli[position].cena + "EUR"
        if (preuzetiModeli[position].image != 0) {
            holder.view.slika.setImageResource(preuzetiModeli[position].image)
        } else {
            holder.view.slika.setImageResource(DataGlobal.logo)
        }
        holder.view.modelLayoutMain.setOnClickListener { view ->
            rowIndex = position
            val intent = Intent(view.context, DetailsActivity::class.java)
            DataGlobal.marka = preuzetiModeli[position].marka
            DataGlobal.model = preuzetiModeli[position].model


            // println("Marka je" + DataGlobal.model)

            intent.putExtra("DETAILS", preuzetiModeli[position])
            println("preuzeti model" + preuzetiModeli[position].toString())
            view.context.startActivity(intent)
        }


    }

    override fun getItemCount(): Int {
        return preuzetiModeli.count()
    }

}
