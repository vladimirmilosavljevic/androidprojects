package com.eng.jsonvezbaapi

object DataGlobal {
    data class Meta(

        val totalPages: Int,
        val currentPage: Int,
        val nextPage: Int,
        val perPage: Int,
        val totalCount: Int,

    )

    data class Team(

        val id: Int,
        val abbrevation: String,
        val city: String,
        val conference: String,
        val division: String,
        val fullName: String,
        val name: String

    )

    data class Data(
        val id: Int,
        val firstName: String,
        val heighFeet: String?,
        val heightInches: String?,
        val lastName: String,
        val position: String,
        val weightPounds: String?,
        val team: Team

    )


       // val dataList: ArrayList<Data> = arrayListOf(),

    val metaList:ArrayList<Meta> = arrayListOf()

    var dataList: ArrayList<Data> = arrayListOf()

    val filterList : ArrayList<Data> = arrayListOf()

    //val info:ArrayList<Info> = arrayListOf()

}