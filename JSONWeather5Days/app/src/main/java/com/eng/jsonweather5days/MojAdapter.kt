package com.eng.jsonweather5days

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.moja_celija.view.*

class MojAdapter(val mainObject: MainWeatherObject) : RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.moja_celija, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.datum.text = mainObject.list[position].dtTxt
        holder.view.tvcity.text = mainObject.city.name
        holder.view.tvCountry.text = mainObject.city.country
        holder.view.tvTemp.text =
            (mainObject.list[position].main.temp  / 17.7).toInt().toString() + "C"
        holder.view.tvPopulation.text = mainObject.city.population.toString() + " people"
        holder.view.tvFeelsLike.text =
            "Feels Like: " + (mainObject.list[position].main.feelsLike / 17.77).toInt()
                .toString() + "C"
        holder.view.tvTempMin.text =
            "MIN: " + (mainObject.list[position].main.tempMin / 17.77).toInt().toString() + "C"
        holder.view.tvTempMax.text =
            "MAX: " + (mainObject.list[position].main.tempMax / 17.77).toInt().toString() + "C"
        holder.view.tvPressure.text =
            "Pressure: " + mainObject.list[position].main.pressure.toString()
        holder.view.tvSeaLevel.text =
            "Sea Level:" + mainObject.list[position].main.seaLevel.toString()
        holder.view.tvHumidity.text =
            "Humidity:" + mainObject.list[position].main.humidity.toString()
        holder.view.tvWeatherMain.text = mainObject.list[position].weather[0].main
        holder.view.tvDescription.text = mainObject.list[position].weather[0].description
        holder.view.tvVisible.text = mainObject.list[position].visibility.toString()
        holder.view.tvCloudsAll.text = mainObject.list[position].clouds.all.toString() + "%"
        holder.view.tvSpeed.text = mainObject.list[position].wind.speed.toString()
        holder.view.tvDeg.text = mainObject.list[position].wind.deg.toString() + "m/s"
        holder.view.tvGust.text = mainObject.list[position].wind.gust.toString()
        holder.view.tvTimezone.text = "Timezone: " + mainObject.city.timezone.toString()


        holder.view.tvLon.text = "Longitude: " + mainObject.city.coord.lon.toString()
        holder.view.tvLat.text = "Latittude: " + mainObject.city.coord.lat.toString()
        holder.view.tvTimezone.text = "Timezone: " + mainObject.city.timezone.toString()

    }

    override fun getItemCount(): Int {
        return mainObject.list.count()
    }
}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {


}