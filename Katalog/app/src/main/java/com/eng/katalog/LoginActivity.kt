package com.eng.katalog

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

val userConst = "user"
val passConst = "user"

class LoginActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        val dugme = findViewById<Button>(R.id.dugme)
        val userText = findViewById<EditText>(R.id.user)
        val passText = findViewById<EditText>(R.id.pass)

        dugme.setOnClickListener {
            if (userText.text.toString() == (userConst) && passText.text.toString() == (passConst))
                startActivity(Intent(this, MainActivity::class.java))

            else {
                Toast.makeText(
                    applicationContext, "Bad username or password",
                    Toast.LENGTH_LONG
                ).show()

            }
            finish()
        }
    }


}