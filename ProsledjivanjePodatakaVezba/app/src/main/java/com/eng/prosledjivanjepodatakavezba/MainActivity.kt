package com.eng.prosledjivanjepodatakavezba

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import com.eng.prosledjivanjepodatakavezba.Podaci.objectArray

import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Serializable

data class Automobil(val automobil: String, val tip: String) :Serializable {

}

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val dugme = findViewById<Button>(R.id.dugme)
        dugme.setOnClickListener {
            parseCSV()
            val intent = (Intent(this, NoviActivity::class.java))
            intent.putExtra("PODACI", objectArray)
            startActivity(intent)
        }
    }

    private fun parseCSV() {
        var linija: String?
        val otvoriCSV = InputStreamReader(assets.open("text.csv"))
        val procitajLiniju = BufferedReader(otvoriCSV)

        while (procitajLiniju.readLine().also {
                linija = it
            } != null) {
            val red: List<String> = linija!!.split(",")
            Log.d("CSV", red.toString())
            if (red[0].isEmpty()) {

                Log.d("CSV", "red je prazan")
            } else {
                Podaci.objectArray.add(Automobil(red[0], red[1]))
            }
        }
    }
}