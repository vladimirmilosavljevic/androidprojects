package com.eng.prosledjivanjepodatakavezba

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.moja_celija.view.*

var rowIndex: Int? = null

class MojAdapter(val preuzetiPodaci: ArrayList<Automobil>) :
    RecyclerView.Adapter<CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.moja_celija, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun getItemCount(): Int {
        return preuzetiPodaci.count()
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.automobil.text = preuzetiPodaci[position].automobil
        holder.view.tip.text = preuzetiPodaci[position].tip
        holder.view.model.setOnClickListener { v ->
            rowIndex = position
            val intent = Intent(v.context, DetailActivity::class.java)
            intent.putExtra("auto", preuzetiPodaci[position].automobil)
            intent.putExtra("tip", preuzetiPodaci[position].tip)
            v.context.startActivity(intent)
        }
    }

}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}