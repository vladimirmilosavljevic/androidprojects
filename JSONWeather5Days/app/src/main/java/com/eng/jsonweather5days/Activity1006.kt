package com.eng.jsonweather5days

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager

import kotlinx.android.synthetic.main.activity_1006.*

class Activity1006 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_1006)

        val kreiraniAdapter = MojAdapter(DataGlobal.filterMain)
        recyclerView1006.adapter = kreiraniAdapter
        recyclerView1006.setLayoutManager(GridLayoutManager(this, 1))
    }
}