package com.eng.katalog

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import kotlinx.android.synthetic.main.activity_details.*
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Serializable

data class Logotip(val ime: String, val slika: Int) : Serializable
data class Image(val ime: String, val slika: Int) : Serializable
class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val dugme = findViewById<Button>(R.id.dugme)
        parseCSV()

        dodajSlike()
        dugme.setOnClickListener {

            startActivity(Intent(this, LogotipActivity::class.java))
        }
    }


    private fun parseCSV() {
        var linija: String?
        val otvoriCSV = InputStreamReader(assets.open("modelExcelNew.csv"))
        val procitajLiniju = BufferedReader(otvoriCSV)
        procitajLiniju.readLine()
        while (procitajLiniju.readLine().also {
                linija = it
            } != null) {
            val red: List<String> = linija!!.split(";")
            Log.d("CSV", red.toString())
            if (red[0].isEmpty()) {

                Log.d("CSV", "red je prazan")
            } else {
                DataGlobal.cars.add(
                    Model(
                        red[0],
                        red[1],
                        red[4],
                        red[2],
                        red[3],
                        red[5],
                        red[6],
                        red[7],
                        red[8],
                        red[9],
                        red[10],
                        red[11],
                        red[12],
                        red[13],
                        red[14],
                        red[15],
                        red[16],
                        red[17],
                        red[18],
                        red[19],
                        red[20],
                        red[21],
                        red[22],
                        red[23],
                        red[24],
                        red[25],
                        red[26],
                        red[27],
                        red[28],
                        red[29],
                        red[30],
                        red[31],
                        red[32],
                        red[33],
                        red[34],
                        red[35],
                        red[36],
                        red[37],
                        red[38],
                        red[39],
                        red[40],
                        red[41],
                        red[42],
                        red[43],
                        red[44],
                        red[45],
                        red[46],
                        red[47],
                        red[48],
                        red[49],
                        red[50],
                        red[51],
                        red[52],
                        red[53],
                        red[54],
                        red[55],
                        red[56],
                        red[57],
                        red[58],
                        red[59],
                        red[60],
                        0
                    )
                )
            }
        }
    }

    fun dodajSlike() {
        DataGlobal.images.add(Image("Giulia", R.drawable.giulia))
        DataGlobal.images.add(Image("A1 sportback (2018)", R.drawable.a1sportback))
        DataGlobal.images.add(Image("C1", R.drawable.c1))
        DataGlobal.images.add(Image("500", R.drawable.f500))
        DataGlobal.images.add(Image("C3", R.drawable.c3))
        DataGlobal.images.add(Image("S-Max", R.drawable.smax))

    }
}