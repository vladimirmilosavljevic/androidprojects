package com.eng.jsonweather5days

import android.icu.number.IntegerWidth
import java.math.BigInteger


object DataGlobal {
    var mainWeatherObj = MainWeatherObject()
    var listOfList: ArrayList<List> = arrayListOf()
    var weatherList: ArrayList<Weather> = arrayListOf()


    val date1006 = "2021-06-10"
    val date1106 = "2021-06-11"
    val date1206 = "2021-06-12"
    val date1306 = "2021-06-13"
    val date1406 = "2021-06-14"

    var filterMain = MainWeatherObject()

}


class Coord
    (
    val lon: Double = 0.0,
    val lat: Double = 0.0

)

class City
    (
    val idCity: Int = 0,
    val name: String = "",
    val coord: Coord = Coord(),
    val country: String = "",
    val population:Int=0,
    val timezone: Int  = 0,
    val sunrise: Int = 0,
    val sunset: Int = 0,


)

class Main(

    var temp: Double = 0.0,
    var feelsLike: Double = 0.0,
    var tempMin: Double = 0.0,
    var tempMax: Double = 0.0,
    var pressure: Int = 0,
    var seaLevel: Int = 0,
    var grndLevel: Int = 0,
    var humidity: Int = 0,
    var tempKf: Int = 0

)

class Weather
    (
    var id: Int = 0,
    var main: String = "",
    var description: String = "",
    var icon: String = "",

    )

class Clouds(
    var all: Int = 0
)

class Wind(

    var speed: Double = 0.0,
    var deg: Int = 0,
    var gust: Double = 0.0
)

class Sys(
    var pod: String = ""
)


class List(
    var dt: String = "",
    var main: Main = Main(),
    var weather: ArrayList<Weather> = arrayListOf(),
    var clouds: Clouds = Clouds(),
    var wind: Wind = Wind(),
    var visibility: Int = 0,
    var pop: Int = 0,
    var sys: Sys = Sys(),
    var dtTxt: String = ""
)
