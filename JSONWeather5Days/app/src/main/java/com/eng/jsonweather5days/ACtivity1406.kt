package com.eng.jsonweather5days

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_1106.*
import kotlinx.android.synthetic.main.activity_1106.recyclerView1106
import kotlinx.android.synthetic.main.activity_1406.*

class ACtivity1406 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_1406)

        val kreiraniAdapter = MojAdapter(DataGlobal.filterMain)

        recyclerView1406.adapter = kreiraniAdapter
        recyclerView1406.setLayoutManager(GridLayoutManager(this, 1))
    }
}