package com.eng.prosledjivanjepodatakavezba

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Layout
import android.widget.LinearLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_novi.*
import kotlinx.android.synthetic.main.moja_celija.*

class NoviActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_novi)
        var prenetaVrednost: ArrayList<Automobil> =
            intent.getSerializableExtra("PODACI") as ArrayList<Automobil>

        val kreiraniAdapter = MojAdapter(prenetaVrednost)
        recyclerView.adapter = kreiraniAdapter
        recyclerView.setLayoutManager(GridLayoutManager(this, 1))
    }
}