package com.eng.jsonvezbaapi

import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL
import java.util.*
import kotlin.collections.ArrayList

var mojAdapter: MojAdapter? = null

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val jsonURl = "https://www.balldontlie.io/api/v1/players"
        preuzmiJSON().execute(jsonURl)

        metaPodaci()

        //adapter

        mojAdapter = MojAdapter(DataGlobal.dataList)
        val recyclerView = findViewById<RecyclerView>(R.id.recycleView)
        recyclerView.adapter = mojAdapter
        recyclerView.setLayoutManager(GridLayoutManager(this, 2))


        val unostTeksta = findViewById<EditText>(R.id.searchEditText)

        unostTeksta.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                filterIgraca(s.toString())
            }

        })

    }

    inner class preuzmiJSON : AsyncTask<String, String, String>() {
        override fun doInBackground(vararg params: String?): String {
            var json: String
            val conn = URL(params[0]).openConnection() as HttpURLConnection

            try {
                conn.connect()
                json = conn.inputStream.use {
                    it.reader().use { reader ->
                        reader.readText()
                    }
                }
            } finally {
                conn.disconnect()
            }
            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            println(result)
            ispisiPodatke(result)

        }
    }

    fun ispisiPodatke(preuzetiJson: String?) {

        val jsonObjekat = JSONObject(preuzetiJson)

        //meta object
        val jsonMeta = jsonObjekat.getJSONObject("meta")

        val totalPages = jsonMeta.getInt("total_pages")
        val currentPage = jsonMeta.getInt("current_page")
        val nextPage = jsonMeta.getInt("next_page")
        val perPage = jsonMeta.getInt("per_page")
        val totalCount = jsonMeta.getInt("total_count")

        DataGlobal.metaList.add(
            DataGlobal.Meta(
                totalPages,
                currentPage,
                nextPage,
                perPage,
                totalCount
            )
        )


        //data array
        val dataArray = jsonObjekat.getJSONArray("data")
        dataArray.let {
            (0 until it.length()).forEach {

                val jsonDataObject = dataArray.getJSONObject(it)
                val id = jsonDataObject.getInt("id")
                val firstName = jsonDataObject.getString("first_name")
                val heighfeet = jsonDataObject.getString("height_feet")
                val heughtinches = jsonDataObject.getString("height_inches")
                val last_name = jsonDataObject.getString("last_name")
                val position = jsonDataObject.getString("position")
                val weightPounds = jsonDataObject.getString("weight_pounds")

                //team object
                val jsonTeamObject = jsonDataObject.getJSONObject("team")
                val idTeam = jsonTeamObject.getInt("id")
                val abbrevation = jsonTeamObject.getString("abbreviation")
                val city = jsonTeamObject.getString("city")
                val conference = jsonTeamObject.getString("conference")
                val division = jsonTeamObject.getString("division")
                val fullName = jsonTeamObject.getString("full_name")
                val name = jsonTeamObject.getString("name")
                val team: DataGlobal.Team =
                    DataGlobal.Team(idTeam, abbrevation, city, conference, division, fullName, name)

                DataGlobal.dataList.add(
                    DataGlobal.Data(
                        id,
                        firstName,
                        heighfeet,
                        heughtinches,
                        last_name,
                        position,
                        weightPounds,
                        team
                    )
                )

                //    DataGlobal.info.add(DataGlobal.Info(DataGlobal.dataList))


            }


        }
    }

    private fun filterIgraca(unos: String) {
        val filterIgraca = ArrayList<DataGlobal.Data>()

        for (data in DataGlobal.dataList) {
            DataGlobal.dataList = DataGlobal.filterList
            if (data.lastName.lowercase().contains(unos.lowercase())) {
                filterIgraca.add(data)

            }
        }
        mojAdapter = MojAdapter(filterIgraca)
        mojAdapter!!.notifyDataSetChanged()
        val recyclerView = findViewById<RecyclerView>(R.id.recycleView)
        recyclerView.adapter = mojAdapter
        recyclerView.setLayoutManager(GridLayoutManager(this, 2))

    }

    fun metaPodaci() {
        for (i in DataGlobal.metaList) {

            /* for ( d in i.data){
                 println(d.team.)
             }*/

            val totalPage = findViewById<TextView>(R.id.totalPages)
            val current = findViewById<TextView>(R.id.currentPage)
            val nextP = findViewById<TextView>(R.id.nextPage)
            val perPage = findViewById<TextView>(R.id.perPage)
            val totalCount = findViewById<TextView>(R.id.total_count)
            val slika = findViewById<ImageView>(R.id.slika)

            totalPage.text = "Total pages:" + i.totalPages.toString()
            current.text = "Current page:" + i.currentPage.toString()
            nextP.text = "Next page:" + i.nextPage.toString()
            perPage.text = "Per page:" + i.perPage.toString()
            totalCount.text = "Total count:" + i.totalCount.toString()
            slika.setImageResource(R.drawable.nba)

        }
    }
}
