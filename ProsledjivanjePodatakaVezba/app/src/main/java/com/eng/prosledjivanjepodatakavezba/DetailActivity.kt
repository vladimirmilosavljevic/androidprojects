package com.eng.prosledjivanjepodatakavezba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import com.eng.prosledjivanjepodatakavezba.Podaci.ime

import com.eng.prosledjivanjepodatakavezba.Podaci.tipAuta
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        ime = intent.getStringExtra("auto")
        tipAuta = intent.getStringExtra("tip")

        tv.text =  ime.toString()
        tv1.text = tipAuta.toString()
    }
}