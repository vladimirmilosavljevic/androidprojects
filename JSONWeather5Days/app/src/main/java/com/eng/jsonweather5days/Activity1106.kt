package com.eng.jsonweather5days

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_1106.*

class Activity1106 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_1106)

        val kreiraniAdapter = MojAdapter(DataGlobal.filterMain)
        recyclerView1106.adapter = kreiraniAdapter
        recyclerView1106.setLayoutManager(GridLayoutManager(this, 1))
    }
}