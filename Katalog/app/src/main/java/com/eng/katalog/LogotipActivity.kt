package com.eng.katalog

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_logotip.*
import kotlinx.android.synthetic.main.moja_celija_logotip.*
import java.io.Serializable


val objectArray: ArrayList<Logotip> = arrayListOf()
var isEmpty: Int = 0

class LogotipActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_logotip)


        createDataSource()

        val search = findViewById<Button>(R.id.search)
        val text = findViewById<EditText>(R.id.unosTv)

        search.setOnClickListener {
            for (car in DataGlobal.cars) {
                var txt = text.text.toString().lowercase()
                var match = car.model.lowercase() + " " + car.marka.lowercase()
                if (match.contains(txt)) {
                    DataGlobal.filteredCars.add(car)
                    ++isEmpty
                }
            }
            for (fil in DataGlobal.filteredCars) {
                for (obj in objectArray) {
                    if (fil.model == obj.ime){
                        DataGlobal.logo = obj.slika
                }

                }

            }
            startActivity(Intent(this, ModelActivity::class.java))
        }

        Log.d("kola filter", "Kola su" + DataGlobal.filteredCars.toString())


        val kreiraniAdapter = MojAdapterLogotip(objectArray)
        recyclerView.adapter = kreiraniAdapter
        recyclerView.setLayoutManager(GridLayoutManager(this, 2))


    }

    private fun createDataSource() {
        objectArray.add(Logotip("Alfa Romeo", R.drawable.alfa))
        objectArray.add(Logotip("Audi", R.drawable.auudi))
        objectArray.add(Logotip("Citroen", R.drawable.citroen))
        objectArray.add(Logotip("Dacia", R.drawable.dacia))
        objectArray.add(Logotip("Fiat", R.drawable.fiat))
        objectArray.add(Logotip("Ford", R.drawable.ford))
        objectArray.add(Logotip("Honda", R.drawable.honda))
        objectArray.add(Logotip("Hyundai", R.drawable.hyundai))
        objectArray.add(Logotip("Infiniti", R.drawable.infiniti))
        objectArray.add(Logotip("Isuzu", R.drawable.jeep))
        objectArray.add(Logotip("Lada", R.drawable.lada))
        objectArray.add(Logotip("Mazda", R.drawable.mazda))
        objectArray.add(Logotip("Mercedes-Benz", R.drawable.mercedes))
        objectArray.add(Logotip("Mini", R.drawable.mini))
        objectArray.add(Logotip("Nissan", R.drawable.nissan))
        objectArray.add(Logotip("Opel", R.drawable.opel))
        objectArray.add(Logotip("Peugeot", R.drawable.peugeot))
        objectArray.add(Logotip("Renault", R.drawable.renault))
        objectArray.add(Logotip("Seat", R.drawable.seat))
        objectArray.add(Logotip("Skoda", R.drawable.skoda))
        objectArray.add(Logotip("Smart", R.drawable.smart))
        objectArray.add(Logotip("Suzuki", R.drawable.suzuki))
        objectArray.add(Logotip("Volkswagen", R.drawable.volkswagen))
        objectArray.add(Logotip("Volvo", R.drawable.volvo))


    }


}