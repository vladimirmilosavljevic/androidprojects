package com.eng.katalog

import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.moja_celija_logotip.*
import kotlinx.android.synthetic.main.moja_celija_logotip.view.*

var rowIndex: Int? = null

class MojAdapterLogotip(val preuzetiModeli: ArrayList<Logotip>) :
    RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.moja_celija_logotip, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.view.naziv.text = preuzetiModeli[position].ime
        holder.view.logo.setImageResource(preuzetiModeli[position].slika)
        holder.view.model.setOnClickListener { view ->
            rowIndex = position
            val intent = Intent(view.context, ModelActivity::class.java)
            DataGlobal.logo = preuzetiModeli[position].slika
            DataGlobal.model = preuzetiModeli[position].ime
            view.context.startActivity(intent)

        }
    }

    override fun getItemCount(): Int {
        return preuzetiModeli.count()
    }


}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}