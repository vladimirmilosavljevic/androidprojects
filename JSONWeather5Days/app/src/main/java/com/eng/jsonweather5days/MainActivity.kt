package com.eng.jsonweather5days

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.io.Serializable
import java.net.HttpURLConnection
import java.net.URL
import kotlin.collections.ArrayList


class MainWeatherObject(


    var cod: String = "",
    var message: Int = 0,
    var cnt: Int = 0,
    val list: ArrayList<List> = arrayListOf(),
    var city: City = City()

) : Serializable

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var grad = "Moscow"

        val jsonURl =
            "https://api.openweathermap.org/data/2.5/forecast?q="+grad+"&appid=356292f8304f3c240d87417fc743ddb0"
        preuzmiJSON().execute(jsonURl)


        btn1006.setOnClickListener {
            startActivity(Intent(this, Activity1006::class.java))
            filterData(DataGlobal.date1006)
        }
        btn1106.setOnClickListener {
            startActivity(Intent(this, Activity1106::class.java))
            filterData(DataGlobal.date1106)
        }
        btn1206.setOnClickListener {
            startActivity(Intent(this, Activity1206::class.java))
            filterData(DataGlobal.date1206)
        }
        btn1306.setOnClickListener {
            startActivity(Intent(this, Activity1306::class.java))
            filterData(DataGlobal.date1306)
        }
        btn1406.setOnClickListener {
            startActivity(Intent(this, ACtivity1406::class.java))
            filterData(DataGlobal.date1406)

        }

    }


    inner class preuzmiJSON : AsyncTask<String, String, String>() {
        override fun doInBackground(vararg params: String?): String {
            var json: String
            val conn = URL(params[0]).openConnection() as HttpURLConnection

            try {
                conn.connect()
                json = conn.inputStream.use {
                    it.reader().use { reader ->
                        reader.readText()
                    }
                }
            } finally {
                conn.disconnect()
            }
            return json
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            println(result)
            ispisiPodatke(result)

        }
    }

    fun ispisiPodatke(preuzetiJson: String?) {

        //  val jsonniz = JSONArray(preuzetiJson)


        val jsonObjekat = JSONObject(preuzetiJson)

        //main  weather objekat

        val cod = jsonObjekat.getString("cod")
        val message = jsonObjekat.getInt("message")
        val cnt = jsonObjekat.getInt("cnt")


        val listObj = jsonObjekat.getJSONArray("list")

        listObj.let {
            (0 until it.length()).forEach {

                val listArray = listObj.getJSONObject(it)

                val dt = listArray.getString("dt")


                //main obj u listi
                //    val jsonMainObj = listObj.getJSONObject(it)
                val jsonMainObj = listArray.getJSONObject("main")
                val temp = jsonMainObj.getDouble("temp")
                val feelsLike = jsonMainObj.getDouble("feels_like")
                val tempMin = jsonMainObj.getDouble("temp_min")
                val tempMax = jsonMainObj.getDouble("temp_max")
                val pressure = jsonMainObj.getInt("pressure")
                val seaLevel = jsonMainObj.getInt("sea_level")
                val grndLevel = jsonMainObj.getInt("grnd_level")
                val humidity = jsonMainObj.getInt("humidity")
                val tempKf = jsonMainObj.getInt("temp_kf")
                val mainObject: Main = Main(
                    temp,
                    feelsLike,
                    tempMin,
                    tempMax,
                    pressure,
                    seaLevel,
                    grndLevel,
                    humidity,
                    tempKf
                )

                //weather array
                val jsonWeatherArray = listArray.getJSONArray("weather")

                jsonWeatherArray.let {
                    (0 until it.length()).forEach {
                        val jsonWeatherObject = jsonWeatherArray.getJSONObject(it)
                        val id = jsonWeatherObject.getInt("id")
                        val main = jsonWeatherObject.getString("main")
                        val description = jsonWeatherObject.getString("description")
                        val icon = jsonWeatherObject.getString("icon")

                        val weatherObject = Weather(id, main, description, icon)
                        DataGlobal.weatherList.add(weatherObject)
                    }
                }

                //json clouds
                val jsonClouds = listArray.getJSONObject("clouds")
                val all = jsonClouds.getInt("all")
                val clouds: Clouds = Clouds(all)


                //json wind
                val jsonWind = listArray.getJSONObject("wind")
                val speed = jsonWind.getDouble("speed")
                val deg = jsonWind.getInt("deg")
                val gust = jsonWind.getDouble("gust")


                //sys
                val jsonSys = listArray.getJSONObject("sys")
                val pod = jsonSys.getString("pod")
                val w: Wind = Wind(speed, deg, gust)

                val visibility = listArray.getInt("visibility")
                val pop = listArray.getInt("pop")
                val dtTxt = listArray.getString("dt_txt")


                val sysObject: Sys = Sys(pod)

                val listObject: List = List(
                    dt,
                    mainObject,
                    DataGlobal.weatherList,
                    clouds,
                    w,
                    visibility,
                    pop,
                    sysObject,
                    dtTxt
                )
                DataGlobal.listOfList.add(listObject)
            }
        }

        //city objekat
        val cityJson = jsonObjekat.getJSONObject("city")
        val idC = cityJson.getInt("id")
        val name = cityJson.getString("name")
        val country = cityJson.getString("country")
        val population = cityJson.getInt("population")
        val timezone = cityJson.getInt("timezone")
        val sunrise = cityJson.getInt("sunrise")
        val sunset = cityJson.getInt("sunset")

        //coord objekat
        val coordJSON = cityJson.getJSONObject("coord")
        val lat = coordJSON.getDouble("lat")
        val lon = coordJSON.getDouble("lon")
        val coordObject: Coord = Coord(lat, lon)

        val cityObject: City =
            City(idC, name, coordObject, country, population, timezone, sunrise, sunset)

        DataGlobal.mainWeatherObj =
            MainWeatherObject(cod, message, cnt, DataGlobal.listOfList, cityObject)


    }

    fun filterData(filterDate: String) {
        DataGlobal.filterMain = MainWeatherObject()
        for (m in DataGlobal.mainWeatherObj.list) {
            if (m.dtTxt.contains(filterDate)) {
                DataGlobal.filterMain.city = DataGlobal.mainWeatherObj.city
                DataGlobal.filterMain.cnt = DataGlobal.mainWeatherObj.cnt
                DataGlobal.filterMain.cod = DataGlobal.mainWeatherObj.cod
                DataGlobal.filterMain.message = DataGlobal.mainWeatherObj.message
                DataGlobal.filterMain.list.add(m)
            }

        }
    }

    fun hideKeyboard(view: View) {
        val zatvori = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        zatvori.hideSoftInputFromWindow(view.windowToken, 0)

    }

}