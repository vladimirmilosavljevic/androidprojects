package com.eng.gesturevezba

import android.graphics.Color
import android.media.Image
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.GestureDetector
import android.view.MotionEvent
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import kotlin.math.abs

class MainActivity : AppCompatActivity(), GestureDetector.OnGestureListener {

    var nizSlika: ArrayList<Int> = arrayListOf()
    var brojac = 0;
    val nizBoja: ArrayList<String> = arrayListOf()


    lateinit var gestureDetector: GestureDetector
    lateinit var imageView: ImageView

    var swipeColor = false

    var x1: Float = 0.0f
    var x2: Float = 0.0f
    var y1: Float = 0.0f
    var y2: Float = 0.0F

    val MIN_DISTANCA = 150


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        gestureDetector = GestureDetector(this, this)


        nizBoja.add("#F5F5DC")
        nizBoja.add("#A52A2A")
        nizBoja.add("#0000FF")


    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event!!.action) {
            0 -> {
                x1 = event.x
                y1 = event.y
            }
            1 -> {
                x2 = event.x
                y2 = event.y

                val vrednostX: Float = x2 - x1
                val vrednostY: Float = y2 - y1

                if (abs(vrednostX) > MIN_DISTANCA) {
                    if (x2 > x1) {
                        Toast.makeText(this, "dogodio se swipe desno", Toast.LENGTH_SHORT).show()
                        if (brojac < 1)
                            brojac = 4
                        imageView.setImageResource(nizSlika[--brojac])

                    } else {
                        Toast.makeText(this, "dogodio se swipe levo", Toast.LENGTH_SHORT).show()
                        if (brojac > 2)
                            brojac = -1
                        imageView.setImageResource(nizSlika[++brojac])

                    }
                } else if (abs(vrednostY) > MIN_DISTANCA) {
                    if (y2 > y1) {
                        Toast.makeText(
                            this,
                            "sliku mozete ubaciti samo swipe-om na gore",
                            Toast.LENGTH_SHORT
                        ).show()
                        menjajBoje()

                    } else {
                        swipeColor = true
                        nizSlika.add(R.drawable.slika)
                        nizSlika.add(R.drawable.slika1)
                        nizSlika.add(R.drawable.slika2)
                        nizSlika.add(R.drawable.slika3)

                        imageView.setImageResource(nizSlika[0])

                        Toast.makeText(this, "dogodio se swipe gore", Toast.LENGTH_SHORT).show()
                    }

                }
            }
        }
        return gestureDetector.onTouchEvent(event)
    }


    override fun onDown(e: MotionEvent?): Boolean {
        return false
    }

    override fun onShowPress(e: MotionEvent?) {

    }

    override fun onSingleTapUp(e: MotionEvent?): Boolean {
        return false
    }

    override fun onScroll(
        e1: MotionEvent?,
        e2: MotionEvent?,
        distanceX: Float,
        distanceY: Float
    ): Boolean {
        return false
    }

    override fun onLongPress(e: MotionEvent?) {
        val layout = findViewById<LinearLayout>(R.id.layout)
        imageView = ImageView(this)
        imageView.layoutParams = LinearLayout.LayoutParams(1500, 600)
        imageView.setBackgroundColor((Color.parseColor("#990000")))
        layout.addView(imageView)
    }

    override fun onFling(
        e1: MotionEvent?,
        e2: MotionEvent?,
        velocityX: Float,
        velocityY: Float
    ): Boolean {
        return false
    }

    var bojeCounter = 0

    fun menjajBoje() {

        if (!swipeColor) {
            Handler(Looper.getMainLooper()).postDelayed({
                imageView.setBackgroundColor(Color.parseColor(nizBoja[bojeCounter]))
                bojeCounter++
                if (bojeCounter == 3) {
                    bojeCounter = 0
                }
                menjajBoje()
            }, 500)

        }

    }

    fun prikaziSledecu(): Int {
        if (brojac == nizSlika.size) {
            brojac = 0
        }
        val image = nizSlika[brojac]
        brojac += 1
        return image


    }

}