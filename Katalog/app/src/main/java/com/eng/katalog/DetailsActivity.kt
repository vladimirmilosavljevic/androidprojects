package com.eng.katalog

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_details.*


class DetailsActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        detailsPodaci()
        hideShowLayout()


    }

    fun hideShowLayout() {
        val dugme = findViewById<Button>(R.id.hideDugme)
        val layout = findViewById<LinearLayout>(R.id.detailsLayout)

        dugme.setOnClickListener {
            if (layout.visibility == View.VISIBLE) {
                layout.visibility = View.GONE
            } else {
                layout.visibility = View.VISIBLE
            }
        }
    }

    fun detailsPodaci() {
        var prenetaVrednost: Model =
            intent.getSerializableExtra("DETAILS") as Model

        if (prenetaVrednost.image == 0) {
            modelDetails.setImageResource(DataGlobal.logo)
        } else {
            modelDetails.setImageResource(prenetaVrednost.image)
        }
        logotipDetails.setImageResource(DataGlobal.logo)
        tvCena.text = prenetaVrednost.cena + "EUR"
        tvMotor.text = "Motor:" + prenetaVrednost.Motor
        tvBrojVentila.text = "Broj ventila:" + prenetaVrednost.brojVentila
        tvoPaket.text = "Paket opreme:" + prenetaVrednost.PaketOpreme
        tvRaspored.text = "Raspored cilindara:" + prenetaVrednost.rasporedCilindara
        tvBrojVentila.text = "Broj ventila:" + prenetaVrednost.brojVentila
        tvPrecnik.text = "Precnik hod klipa:" + prenetaVrednost.precnikHodKlipa
        tvTipUbr.text = "Tip ubrizgavanja:" + prenetaVrednost.tipUbrizgavanja
        tvSistemOtvaranjaVentila.text =
            "Sistem otvaranja ventila:" + prenetaVrednost.sistemOtvaranjaVentila
        tvTurbo.text = "Turbo:" + prenetaVrednost.turbo
        tvZapremina.text = "Zapremina:" + prenetaVrednost.zapreminaMotora
        tvKw.text = "KW:" + prenetaVrednost.kw
        tvKs.text = "KS:" + prenetaVrednost.ks
        tvSnaga.text = "Snaga pri obrtajima:" + prenetaVrednost.snagaPriObrtajima
        tvObrtni.text = "Obrtni moment:" + prenetaVrednost.obrtniMoment
        tvObrtniPri.text =
            "Obrtni moment pri obrtajima:" + prenetaVrednost.obrtniMomenatPriObrtajima
        tvStepen.text = "Stepen kompresije:" + prenetaVrednost.stepenKompresije
        tvTipMenjaca.text = "Tip menjaca:" + prenetaVrednost.tipMenjaca
        tvBrojStepeni.text = "Broj stepeni prenosa:" + prenetaVrednost.brojStepeniPrenosa
        tvPogon.text = "Pogon:" + prenetaVrednost.pogon
        tvDuzina.text = "Duzina:" + prenetaVrednost.duzina
        tvSirina.text = "Sirina:" + prenetaVrednost.sirina
        tvVisina.text = "Visina:" + prenetaVrednost.visina
        tvMedjuosovinsko.text =
            "Medjuosovinsko rastojanje:" + prenetaVrednost.medjuosovinskoRastojanje
        tvTezinaPraznog.text = "Tezina praznog vozila:" + prenetaVrednost.tezinaPraznogVozila
        tvMaksDozvoljenaTezina.text = "Maks dozvoljena tezina:" + prenetaVrednost.maksimalnaTezina
        tvZapreminaRez.text = "Zapremina rezervoara:" + prenetaVrednost.zapreminaRezervoara
        tvZaprPrt.text = "Zapremina prtljaznika:" + prenetaVrednost.zapreminaPrtljaznika
        tvMaksZaprPrt.text = "Maks zapremina prtljaznika:" + prenetaVrednost.maksZapreminaPrtlj
        tvDozvoljenTovar.text = "Dozvoljen tovar:" + prenetaVrednost.dozvoljenTovar
        tvDozvoljenoOpterecenjeKrova.text =
            "Dozvoljeno opterecenje krova:" + prenetaVrednost.dozvoljenoOpterecenjeKrova
        tvDozvoljenaTezinaBK.text =
            "Dozvoljena tezina prikolice BK:" + prenetaVrednost.dozvoljenaTezinaPrikoliceBK
        tvDozvoljenaTezinaSK12.text =
            "Dozvoljena tezina prikolice SK12:" + prenetaVrednost.dozvoljenaTezinaPrikoliceSK12
        tvDozvoljenaTezinaSK8.text =
            "Dozvoljena tezina prikolice SK8:" + prenetaVrednost.dozvoljenaTezinaPrikoliceSK8
        tvOpterecenjeKuke.text = "Opterecenje kuke:" + prenetaVrednost.opterecenjeKuke
        tvRadijus.text = "Radijus okretanja:" + prenetaVrednost.radijusOkretanja
        tvTragTockova.text = "Trag tockova napred:" + prenetaVrednost.tragTockovaNapred
        tvTragTockovaNazad.text = "Trag tockova nazad:" + prenetaVrednost.tragTockovaNazad
        tvMaksBrzina.text = "Maks brzina:" + prenetaVrednost.maksimalnaBrzina
        tvUbrzanje0100.text = "Ubrzanje 0-100:" + prenetaVrednost.ubrzanje0100
        tvUbrzanje0200.text = "Ubrzanje 0-200:" + prenetaVrednost.ubrzanje0200
        tvUbrzanje80120.text = "Ubrzanje 80-120:" + prenetaVrednost.ubrzanje80120
        tvZaustavni.text = "Zaustavni put:" + prenetaVrednost.zaustavniPut100
        tvVremeZa400m.text = "Vreme za 400m:" + prenetaVrednost.vremeZa400m
        tvPotrosnjaGrad.text = "Potrosnja grad:" + prenetaVrednost.potrosnjaGrad
        tvPotrosnjaVGrada.text = "Potrosnja van grada:" + prenetaVrednost.potrosnjaVGrada
        tvEmisija.text = "Emisija C02:" + prenetaVrednost.emisijaCO2
        tvKatalizator.text = "Katalizator:" + prenetaVrednost.katalizator
        tvDimenzijePneu.text = "Dimenzije pneumatika:" + prenetaVrednost.dimenzijePneumatika
        tvPrednjeOpruge.text = "Prednje opruge:" + prenetaVrednost.prednjeOpruge
        tvZadnjeOp.text = "Zadnje opruge:" + prenetaVrednost.zadnjeOpruge
        tvPrednjiStabil.text = "Prednji stabilizator:" + prenetaVrednost.prednjiStabilizator
        tvZadnjiStabil.text = "Zadnji stabilizator:" + prenetaVrednost.zadnjiStabilizator
        tvGarancijaKorozija.text = "Garancija korozija:" + prenetaVrednost.garancijaKorozija
        tvGarancijaMotor.text = " Garancija motor:" + prenetaVrednost.garancijaMotor
        tvEuroNCAP.text = "Euro NCAP :" + prenetaVrednost.euroNCAP
        tvEuroNCAPZvezd.text = "Euro NCAP zvezdice:" + prenetaVrednost.euroNCAPzvezdice
        tvGorivo.text = "Gorivo:" + prenetaVrednost.gorivo
        tvBrojVrata.text = "Broj vrata:" + prenetaVrednost.brojVrata
        tvBrojSedista.text = "Broj sedista:" + prenetaVrednost.brojSedista

    }


}
