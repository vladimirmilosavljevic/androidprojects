package com.eng.katalog

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_logotip.*
import kotlinx.android.synthetic.main.activity_logotip.recyclerView
import kotlinx.android.synthetic.main.activity_model.*
import kotlinx.android.synthetic.main.moja_celija_logotip.*
import kotlinx.android.synthetic.main.moja_celija_model.*
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Serializable
import java.nio.channels.DatagramChannel

data class Model(
    val model: String,
    val marka: String,
    val cena: String,
    val Motor: String,
    val PaketOpreme: String,
    val rasporedCilindara: String,
    val brojVentila: String,
    val precnikHodKlipa: String,
    val tipUbrizgavanja: String,
    val sistemOtvaranjaVentila: String,
    val turbo: String,
    val zapreminaMotora: String,
    val kw: String,
    val ks: String,
    val snagaPriObrtajima: String,
    val obrtniMoment: String,
    val obrtniMomenatPriObrtajima: String,
    val stepenKompresije: String,
    val tipMenjaca: String,
    val brojStepeniPrenosa: String,
    val pogon: String,
    val duzina: String,
    val sirina: String,
    val visina: String,
    val medjuosovinskoRastojanje: String,
    val tezinaPraznogVozila: String,
    val maksimalnaTezina: String,
    val zapreminaRezervoara: String,
    val zapreminaPrtljaznika: String,
    val maksZapreminaPrtlj: String,
    val dozvoljenTovar: String,
    val dozvoljenoOpterecenjeKrova: String,
    val dozvoljenaTezinaPrikoliceBK: String,
    val dozvoljenaTezinaPrikoliceSK12: String,
    val dozvoljenaTezinaPrikoliceSK8: String,
    val opterecenjeKuke: String,
    val radijusOkretanja: String,
    val tragTockovaNapred: String,
    val tragTockovaNazad: String,
    val maksimalnaBrzina: String,
    val ubrzanje0100: String,
    val ubrzanje0200: String,
    val ubrzanje80120: String,
    val zaustavniPut100: String,
    val vremeZa400m: String,
    val potrosnjaGrad: String,
    val potrosnjaVGrada: String,
    val emisijaCO2: String,
    val katalizator: String,
    val dimenzijePneumatika: String,
    val prednjeOpruge: String,
    val zadnjeOpruge: String,
    val prednjiStabilizator: String,
    val zadnjiStabilizator: String,
    val garancijaKorozija: String,
    val garancijaMotor: String,
    val euroNCAP: String,
    val euroNCAPzvezdice: String,
    val gorivo: String,
    val brojVrata: String,
    val brojSedista: String,
    var image: Int
) : Serializable

class ModelActivity : AppCompatActivity() {

    val objectArray: ArrayList<Model> = arrayListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_model)



        for (car in DataGlobal.cars) {
            if (car.model == DataGlobal.model) {
                objectArray.add(car)
            }
        }
        for (car in objectArray) {
            for (img in DataGlobal.images) {
                var model = car.marka
                if (model == img.ime) {
                    car.image = img.slika

                }
            }

        }
        for (ob in objectArray) {

            println("Slika je" + ob.image)
        }

        if (isEmpty > 0) {
            val kreiraniAdapter = MojAdapterModel(DataGlobal.filteredCars)
            recycleViewModel.adapter = kreiraniAdapter
            recycleViewModel.setLayoutManager(GridLayoutManager(this, 2))
            logotip.setImageResource(DataGlobal.logo)
        } else {
            val kreiraniAdapter = MojAdapterModel(objectArray)
            recycleViewModel.adapter = kreiraniAdapter
            recycleViewModel.setLayoutManager(GridLayoutManager(this, 2))
            logotip.setImageResource(DataGlobal.logo)
        }
        // println("object array" +objectArray.toString())
        //  println("data global" +DataGlobal.cars.toString())

    }


    override fun onBackPressed() {
        isEmpty = 0
        DataGlobal.filteredCars = arrayListOf()
        super.onBackPressed()

    }


}