package com.eng.pickerview

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.DatePicker
import android.widget.TextView
import com.google.android.material.timepicker.TimeFormat
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    val formatDatuma = SimpleDateFormat("dd-MM-YYYY", Locale.US)
    val formatVremena = SimpleDateFormat("HH:mm", Locale.ITALY)

    var sve = ""
    var datumIVreme = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val dugme = findViewById<Button>(R.id.dugme)

        print(sve)

        dugme.setOnClickListener {
            uzmiVreme()
            uzmiDatum()
            showChangeLang()


        }

    }


    private fun uzmiVreme() {
        val ispis = findViewById<TextView>(R.id.vreme)
        val datum = findViewById<TextView>(R.id.datum)
        val jezik = findViewById<TextView>(R.id.jezik)
        val sadasnjeVreme = Calendar.getInstance()
        val pikerVreme = TimePickerDialog(this, TimePickerDialog.OnTimeSetListener{
                picker, hourOfDay, minute ->
            val sadasnjeVreme = Calendar.getInstance()
            sadasnjeVreme.set(Calendar.HOUR_OF_DAY, hourOfDay)
            sadasnjeVreme.set(Calendar.MINUTE, minute)
            val ispisVremena = formatVremena.format(sadasnjeVreme.time)
            datumIVreme = datumIVreme + " " + ispisVremena
            ispis.text = datumIVreme
                ispis.text = ispisVremena
            val svi = findViewById<TextView>(R.id.svizajedno)

            sve = ispis.text.toString()+ " , " + datum.text.toString() +" , "+ jezik.text.toString()
            svi.text = sve

        },
            sadasnjeVreme.get(Calendar.HOUR_OF_DAY), sadasnjeVreme.get(Calendar.MINUTE), true)
        pikerVreme.show()


    }

    private fun uzmiDatum() {

        val datum = findViewById<TextView>(R.id.datum)
        val jezik = findViewById<TextView>(R.id.jezik)
        val sadasnjeVreme = Calendar.getInstance()
        val pikerDatum = DatePickerDialog(this, DatePickerDialog.OnDateSetListener{
                picker, year, month, dayOfMonth ->
            val sadasnjeVreme = Calendar.getInstance()
            sadasnjeVreme.set(Calendar.YEAR, year)
            sadasnjeVreme.set(Calendar.MONTH, month)
            sadasnjeVreme.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            //Instanca Calendar-a uzima trenutni datum i vreme
            val ispisDatuma = formatDatuma.format(sadasnjeVreme.time)
            datumIVreme = ispisDatuma
           // uzmiVreme()
                datum.text =ispisDatuma
            sve = datum.text.toString() + jezik.text.toString()


        },
            sadasnjeVreme.get(Calendar.YEAR), sadasnjeVreme.get(Calendar.MONTH), sadasnjeVreme.get(Calendar.DAY_OF_MONTH))
        pikerDatum.show()

    }

    private fun showChangeLang() {
        val ispis = findViewById<TextView>(R.id.jezik)

        val nizJezika = arrayOf("Srpski", "Engleski", "Spanski", "Ruski")
        //Pravimo AlertDialog
        val pikerJezici = androidx.appcompat.app.AlertDialog.Builder(this)
        pikerJezici.setTitle("Izaberite jezik...")
        pikerJezici.setSingleChoiceItems(nizJezika, -1) { dialog, jezik ->
            ispis.text = nizJezika[jezik]
            sve = ispis.text.toString()

            dialog.dismiss()
        }
        //Uzmi AlertDialog i prikazi ga
        val prikazi = pikerJezici.create()

        prikazi.show()

    }

}