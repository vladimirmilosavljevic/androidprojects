package com.eng.jsonvezbaapi

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.moja_celija.view.*

class MojAdapter(val preuzetiModeli: ArrayList<DataGlobal.Data>) :
    RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.moja_celija, parent, false)
        return CustomViewHolder(cellForRow)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {

        // player data
        holder.view.idPlayer.text =
            "Player id:" + preuzetiModeli[position].id.toString()
        holder.view.firstname.text =
            "First name:" + preuzetiModeli[position].firstName
        holder.view.height_feet.text =
            "Height FT:" + preuzetiModeli[position].heighFeet.toString()
        holder.view.height_inches.text =
            "Height inches:" + preuzetiModeli[position].heightInches.toString()
        holder.view.last_name.text = "Last name:" + preuzetiModeli[position].lastName
        holder.view.position.text = "Position:" + preuzetiModeli[position].position
        holder.view.weight_pounds.text =
            "Weight:" + preuzetiModeli[position].weightPounds

        // team data
        holder.view.idTeam.text =
            "Team id:" + preuzetiModeli[position].team.id.toString()
        holder.view.abbreaviation.text =
            "Abbreaviation:" + preuzetiModeli[position].team.abbrevation
        holder.view.city.text = "City:" + preuzetiModeli[position].team.city
        holder.view.conference.text =
            "Conference:" + preuzetiModeli[position].team.conference
        holder.view.division.text =
            "Division:" + preuzetiModeli[position].team.division
        holder.view.full_name.text =
            "Full name:" + preuzetiModeli[position].team.fullName
        holder.view.name.text = "Name:" + preuzetiModeli[position].team.name

        holder.view.slikaTeam.setImageResource(R.drawable.nba)
        holder.view.slikaIgrac.setImageResource(R.drawable.nba)


        if (preuzetiModeli[position].lastName == "Anigbogu") {
            holder.view.slikaIgrac.setImageResource(R.drawable.ike)
        }
        if (preuzetiModeli[position].team.name == "Pacers") {
            holder.view.slikaTeam.setImageResource(R.drawable.indiana)
        }

        if (preuzetiModeli[position].lastName == "Baker") {
            holder.view.slikaIgrac.setImageResource(R.drawable.i)
        }
        if (preuzetiModeli[position].team.name == "Knicks") {
            holder.view.slikaTeam.setImageResource(R.drawable.knicks)
        }
        if (preuzetiModeli[position].lastName == "Bird") {
            holder.view.slikaIgrac.setImageResource(R.drawable.bird)
        }
        if (preuzetiModeli[position].team.name == "Celtics") {
            holder.view.slikaTeam.setImageResource(R.drawable.kelti)
        }


    }


    override fun getItemCount(): Int {
        return preuzetiModeli.count()
    }


}

class CustomViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

}

